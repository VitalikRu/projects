<?php

// урок ORM

require __DIR__ . '/autoload.php';
// для вызова динамического метода, нужно создовать объект и через него вызов метода класса
// вызываем статический метод (::) из данного класса и при этом в нём нельзя использовать (this)



$view = new \App\Vue();

//$view->assign('article', App\Models\Article::findAll()); // первый вариант
$view->article = App\Models\Article::findAll(); // второй вариант


$view->display(__DIR__ . '/templates/index.php'); // поеазать эти данные с помощью данного шаблона
var_dump($view->render(__DIR__ . '/templates/index.php')); // выведет значение в виде строки
