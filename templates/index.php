<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>NEVS</h1>
    <hr>
<!-- $this->article это свойство передается в метод __get т к свойство не объявлено-->
    <?php foreach ($this->article as $book) : ?>
    <article>
        <h3><?php echo $book->title; ?></h3>
        <p><?php echo $book->author; ?></p>

    </article>
<?php endforeach; ?>
    <hr>

</body>
</html>
