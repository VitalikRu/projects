<?php


namespace App;

//
abstract class Model
{

    public const TABLE = '';

    public $id;
    // абстактный класс это метод только его заголовок (сигнатура) его реалезация в классах наследниках которые обязаны использовать этот метод, наполнить его содержимым и реалезовать, у абстактного класса не может быть объектов, если в классе есть хоть один абстрактный метод класс должен быть абстрактным
    //abstract public function getModelName();


    //для вызова стаческого метода не нужно создовать объект (убираем все this)
    public static function findAll ()
    {
        // обращение к статическому свойству Article::$table (класс и его статик свойство)
        $db = new Db();

        $sql = 'SELECT * FROM ' . static::TABLE; // выносим запрос для отладки
        // Article::TABLE класс Article необходимо поменять на self обозначает сам класс где это слово находится
        // self раннее связывание, на этапе компиляции | static позднее связывание
        return $db->query($sql, [], static::class); // альтернатива прямого пути '\App\Models\Article' будет (Article::class)
    }


    public function insert()
    {
        $fields = get_object_vars($this);

        $cols = [];
        $data = [];

        foreach($fields as $name => $value){
            if('id' == $name){
                continue;// оператор для пропуска этого значения
            }
            $cols[] = $name;
            $data[':' . $name] = $value;
        }

//        var_dump($cols);

        /*для отладки echo*/ $sql = 'INSERT INTO ' . static::TABLE . ' (' . implode(',', $cols) . ') VALUES (' . implode(',', array_keys($data)) . ')';


        $db = new Db();
        $db->execute($sql, $data);

        $this->id = $db->getLastId();

//        var_dump($data);
    }
}
