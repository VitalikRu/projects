<?php


namespace App;


class Db
{   // protected - Объявление защищенного метода
    protected $dbh;

    public function __construct()
    {
        // соединение с базой данных
        $config = (include __DIR__ . '/../config.php') ['db'];
        //  '\' - корневое пространство имен
        $this->dbh = new \PDO('mysql:host=' . $config['host'] . ';dbname=' . $config['dbname'] , $config['user'], $config['password']);
    }
    // метод query умеет делать sql запрос подставив в него определенные данные
    public function query($sql, $data=[], $class)
    { // query (запрос) - возвращает данные
        $sth = $this->dbh->prepare($sql); // подключили подготовили запрос
        $sth->execute($data); // выполнели подготовленный запрос с подставленными данными
        return $sth->fetchAll(\PDO::FETCH_CLASS, $class); // 1. получили данные из базы
    }

    public function execute($sql, $data=[])
    { // execute (выполнять) исполняет запросы ничего не возвращает
        $sth = $this->dbh->prepare($sql); // подключили подготовили запрос
        return $sth->execute($data); // выполнели подготовленный запрос с подставленными данными


    }

    public function getLastId(){ // метод обёртка
        return $this->dbh->lastInsertId(); //lastInsertId - Возвращает ID последней вставленной строки или значение последовательности
    }
}
