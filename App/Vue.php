<?php


namespace App;


class Vue
{
    protected $data = []; //те данные которые для отоброжения подготовлены

    // публичный метод assign который эти данные в массиве сохроняет (имя и значение)
    public function assign($name, $value){
        $this->data[$name] = $value;
    }

    // set вызывается автомотически если пытаемся записать в несуществуюшие свойство
    public function __set($name, $value){
        $this->data[$name] = $value;
    }

    // любое несуществующее свойство будет равно 42
    public function __get($name){// передаём имя метода который пытаются прочесть
        return $this->data[$name] ?? null; // проверка если не существует вернёт null
    }
    //isset возвращает true или false проверяет свойство на наличие
    public function __isset($name)
    {
        return isset($this->data[$name]);
    }
    // буфер
    public function render($template){
        ob_start(); //функция прекращает даоьнейший вывод потока
        include $template; // вывод шаблона
        $content = ob_get_contents(); //возвращает текушее содержимое буфера
        ob_end_clean(); // разрушели накопившейся поток в буфере пусто
        return $content; // возвращаем значение разрушенного ранее метода выведет в виде строки
    }

    //$template отоброжает шаблон
    public function display($template){
        include $template;
    }
}