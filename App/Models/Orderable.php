<?php


namespace App\Models;

interface Orderable extends HasPrice, HasWeight
{   // третий метод указываем в самом интерфейсе название (getTitle)
    public function getTitle();

}
//интерфейсы имеют множественное наследие
// запись читается - то что можно заказать у чего есть цена (HasPrice) и есть вес (HasWeight)
