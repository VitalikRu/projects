<?php
//ORM - объект это запись, класс это таблица статические методы работают с таблицей в целом, динамические работают с одной записью
//Active Record - запсь в базе данных может сама себя удалить, сохранить, обновить
namespace App\Models;
//сущность под названием скидочная карта

//возможна токая форма записи use App\Model as M;
//class GiftCard extends M implements HasPrice

use App\Model;
// дополнительно к наследованию реалезуется интерфейс
//интерфейсы могут наследоваться друг от друга
class GiftCard extends Model implements HasPrice
{
    public const TABLE = 'cards';

    //добавляем заготовки метода интерфейс,реалезация интерфейса появилось тело метода
    //public function getPrice()// тело метода интерфейса всегда публичное

    use HasPriceExample;
}
