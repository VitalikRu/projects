<?php


namespace App\Models;

//inteface что может быть заказано
interface HasPrice
{   //тело метода у интерфейса нет, реалезуется интерфейс через класс
    public function getPrice(); // вернет цену

}
