<?php


namespace App\Models;

//трейд - это загатовка для класса, интерфейсом это просто код который можно в будущий класс вставить
trait HasPriceExample
{
    protected $price = 20;

    public function getPrice()
    {
        return $this->price;
    }

}
