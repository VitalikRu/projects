<?php
// возвращаем массив нужных нам объектов

namespace App\Models;
// MVC слой модели знают где находятся данные
use App\Model;

class User extends Model
{
    // статичным могут также быть и свойства, обращаться к данному свойству без (this)
    public const TABLE = 'orders';

//    public $id;
    public $title;
    public $author;
    public $pubyear;
    public $price;
    public $quantity;
    public $orderid;
    public $datetime;

    /*--выносим в класс Model вывод таблици из базы данных--*/
//    public function getModelName()
//    {
//        return 'Другой текст!';
//    }
}
