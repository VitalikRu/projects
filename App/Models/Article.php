<?php
// возвращаем массив нужных нам объектов

namespace App\Models;
// MVC слой модели знают где находятся данные
use App\Model;

class Article extends Model
{
    // статичным могут также быть и свойства, обращаться к данному свойству без (this)
    public const TABLE = 'catalog';

//    public $id;
    public $title;
    public $author;
    public $pubyear;
    public $price;

    /*--выносим в класс Model вывод таблици из базы данных--*/
//    public function getModelName()
//    {
//        return 'Просто текст!';
//    }
}
