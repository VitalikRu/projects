<?php


namespace App\Models;

//inteface что может быть заказано
interface HasWeight
{
    public function getWeight();
}
