<?php
// тайп хейтинг контроль типов
require __DIR__ . '/autoload.php';
//  .1. способ function buy(\App\Model $item){
function buy(\App\Models\HasPrice $item){ // чаще всего указывают интерфейс
    var_dump($item);
}

$item = new \App\Models\GiftCard();
buy($item);
